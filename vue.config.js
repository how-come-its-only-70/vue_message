module.exports = {
    devServer: {
        proxy: {
            '/api': {
                
                target: 'https://www.baidu.com/api',
                // 允许跨域
                changeOrigin: true,
                ws: true,
                pathRewrite: {
                    '^/api': ''
                }
            }
        }
    }
}