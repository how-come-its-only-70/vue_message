//express
var express=require('express')
var router = express.Router()
//mysql
var sql=require('../sql')
var connection=null //数据库对象
 
const url=require('url')

// 获得user列表  
router.get('/user',(req,res)=>{
    res.header("Access-Control-Allow-Origin","*")
    connection=sql.createConnection()
    connection.connect()
    connection.query(`select * from user`,function(err,rows){
        if(err) throw err;
        res.send(rows)
    })
    connection.end();
})

// 重构user列表  
router.post('/reuser',(req,res)=>{
    res.header("Access-Control-Allow-Origin","*")
    connection=sql.createConnection()
    connection.connect()
    connection.query(`select * from user where name like '%${req.body.name}%'`,function(err,rows){
        if(err) throw err;
        res.send(rows)
    })
    connection.end();
    console.log(req);
})
//添加user
router.post('/add',(req,res)=>{
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
    res.header("X-Powered-By", ' 3.2.1');
    res.header("Content-Type", "application/json;charset=utf-8");

    connection=sql.createConnection()
    connection.connect()
    connection.query(`insert into user (name,sex,id,location,flog) value ('${req.body.name}','${req.body.sex}','${req.body.id}','${req.body.location}','${req.body.flog}')`,function(err,rows){
        if(err) throw err;
        
    })
    connection.end();

    console.log(req.body);
    
})
//删除用户
router.post('/delate',(req,res)=>{
    
    connection=sql.createConnection()
    connection.connect()
    connection.query(`delete from user where name='${req.body.name}'`,function(err,rows){
        if(err) throw err;
        
    })
    connection.end();
    
})

//获得用户课程列表

router.post('/getclass',(req,res)=>{
    
    connection=sql.createConnection()
    connection.connect()
    connection.query(`select * from ${req.body.flog}`,function(err,rows){
        if(err) throw err;
        res.send(rows)
        console.log(rows);
    })
    connection.end();
})

// 登录选课

router.post('/login',(req,res)=>{

    connection=sql.createConnection()
    connection.connect()
    connection.query(`select name from user where id='${req.body.id}'`,function(err,rows){
        if(rows.length==0){
           
            res.send({
                status:'422',
                message:'学生不存在!'
            })
        }
        else{
            res.send(rows)
        }
       
    })
    connection.end();
})

// 获得课程列表

router.post('/getclasslist',(req,res)=>{
    // let start = (Number(req.body.page)-1)*10
    // let end = (Number(req.body.page)*10)-1
    // console.log(req.body.page);
    let start = (req.body.page-1)*5
    let end = (req.body.page*5)
    connection=sql.createConnection()
    connection.connect()
    connection.query('select * from classlist',function(err,rows){
        let row = rows.slice(start,end)
        if(err) throw err;
        res.send(row)
    })
    connection.end();
})

// 选课

router.post('/choice',(req,res)=>{

    
    let a = req.body.flot
   
    connection=sql.createConnection()
    connection.connect()
    connection.query(`insert into ${a} (classId,className,classPlace,state) value ('${req.body.data.classId}','${req.body.data.className}','${req.body.data.classPlace}','挂科')`,function(err,rows){
        if(err) throw err;
        res.send(rows)
    })
    connection.end();
})

// var user ={
//     flog:req.body.flog
// }

module.exports=router