import Vue from 'vue'
import VueRouter from 'vue-router'
import App from '../App.vue'
import content from '../views/content.vue'
import user from '../components/user.vue'
import classlist from '../components/classlist.vue'
import choice from '../components/choice.vue'
import choicec from '../components/choicec.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: content
  },
 {
   path:'/user',
   component:user
 },
 {
   path:'/class',
   component:classlist
 },
 {
   path:'/choice',
  component:choice,
 },
 {
  path:'/choicec',
  name:'choicec',
  component:choicec
 }

]

const router = new VueRouter({
  routes
})

export default router
